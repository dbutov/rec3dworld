import torch
import torch.nn.functional as F

import random

class DSAC:
	'''
	Differentiable RANSAC to robustly fit lines.
	'''

	def __init__(self, hyps, inlier_thresh, inlier_beta, inlier_alpha, loss_function):
		'''
		Constructor.

		hyps -- number of line hypotheses sampled for each image
		inlier_thresh -- threshold used in the soft inlier count, its measured in relative image size (1 = image width)
		inlier_beta -- scaling factor within the sigmoid of the soft inlier count
		inlier_alpha -- scaling factor for the soft inlier scores (controls the peakiness of the hypothesis distribution)
		loss_function -- function to compute the quality of estimated line parameters wrt ground truth
		'''

		self.hyps = hyps
		self.inlier_thresh = inlier_thresh
		self.inlier_beta = inlier_beta
		self.inlier_alpha = inlier_alpha
		self.loss_function = loss_function

	def __sample_hyp(self, x, y):
		'''
		Calculate a circle hypothesis (cX, cY, r) from three random points.

		x -- vector of x values (range 0 to 1)
		y -- vector of y values (range 0 to 1)
		'''

		# -- STUDENT BEGIN --------------------------------------------------------

		# TASK 4.1.

		# You are given all predicted points of the CNN (x, y)
		# Randomly select three points (but no point twice within this call) and fit a circle 
		# (see the slides for instructions).

		# Check if the radius is below 1 (sanity check). If not, try again. Try 1000 times max.
		
		# You should return four values: x of the circle center, y of the circle center, the radius,
		# and True resp. False if a valid circle has been found within 1000 tries (radius < 1).

		# -- STUDENT END ----------------------------------------------------------

		numPoints = x.size(0)
    
		radius = 2
		a=0
		b=0
		c=0
		cnt=1000        
    
		while radius >= 1 and cnt>0:
    
			cnt -=1     
			x1 = -1
			x2 = -1
			x3 = -1
			y1 = -1
			y2 = -1
			y3 = -1
			idx2 = 0
			idx3 = 0         
			idx1 = 0
            
			while x1<0 or y1<0:
				idx1 = random.randint(0, numPoints-1)
				x1 = x[idx1]
				y1 = y[idx1]
            
            
			while idx2 == idx1 or x2<0 or y2<0 :
				idx2 = random.randint(0, numPoints-1)
				x2 = x[idx2]
				y2 = y[idx2]                
            
			while idx3 == idx1 or idx3 == idx2 or x3<0 or y3<0:
				idx3 = random.randint(0, numPoints-1)        
				x3 = x[idx3]
				y3 = y[idx3]

            
			#print("x1 = " , x1)
			#print("x2 = " , x2)
			#print("x3 = " , x3)
			#print("y1 = " , y1)
			#print("y2 = " , y2)
			#print("y3 = " , y3)
			

			# set matricies
#			aMat = torch.tensor([[x1, y1, 1], [x2, y2, 1], [x3, y3, 1]])
#			aMat = torch.mm(aMat,aMat.t())            
#			print("aMat = " , aMat)
#    
#			bMat = torch.tensor([[x1*x1 + y1*y1, y1, 1], [x2*x2 + y2*y2, y2, 1], [x3*x3 + y3*y3, y3, 1]])
#			bMat = torch.mm(bMat,bMat.t())                
#            
#			cMat = torch.tensor([[x1*x1 + y1*y1, x1, 1], [x2*x2 + y2*y2, x2, 1], [x3*x3 + y3*y3, x3, 1]])
#			cMat = torch.mm(cMat,cMat.t())                
#            
#			dMat = torch.tensor([[x1*x1 + y1*y1, x1, y1], [x2*x2 + y2*y2, x2, y2], [x3*x3 + y3*y3, x3, y3]])
#			dMat = torch.mm(dMat,dMat.t())                
#
#			print("bMat = " , bMat)
#			print("cMat = " , cMat)
#			print("dMat = " , dMat)            
#            
#			# calc determinants
#			a = torch.potrf(aMat).diag().prod()
#			b = -torch.potrf(bMat).diag().prod()
#			c = torch.potrf(cMat).diag().prod()
#			d = -torch.potrf(dMat).diag().prod()
    			
			SQ1 = x1*x1 + y1*y1
			SQ2 = x2*x2 + y2*y2
			SQ3 = x3*x3 + y3*y3
    
			a = x1*(y2-y3) - y1*(x2-x3) + x2*y3 - x3*y2
			b = SQ1 * (y3-y2) + SQ2 * (y1-y3) + SQ3 * (y2-y1)
			c = SQ1 * (x2-x3) + SQ2 * (x3-x1) + SQ3 * (x1-x2)
			d = SQ1 * (x3*y2-x2*y3) + SQ2 * (x1*y3-x3*y1) + SQ3 * (x2*y1-x1*y2)
    
			radius = torch.sqrt( ( b*b + c*c -4*a*d ) / (4*a*a) )
    
		if cnt <= 0:			
			return 0, 0, 0, False
		else:     		  			
			x = -b/(2*a)
			y = -c/(2*a)    
			return x, y, radius, True

	def __soft_inlier_count(self, cX, cY, r, x, y):
		'''
		Soft inlier count for a given circle and a given set of points.

		cX -- x of circle center
		cY -- y of circle center
		r -- radius of the circle
		x -- vector of x values (range 0 to 1)
		y -- vector of y values (range 0 to 1)
		'''

		# -- STUDENT BEGIN --------------------------------------------------------

		# TASK 4.2.

		# You are given the circle parameters cX, cY and r, as well as all predicted points of the CNN (x, y)
		# Calculate the distance of each point to the circle.
		# Turn the distances to soft inlier scores by applying a sigmoid as in the line fitting code.
		# Use the member attributes self.inlier_beta as the scaling factor within the sigmoid,
		# and self.inlier_thresh as the soft inlier threshold (see line fitting code).

		# Note that when using the sqrt() function, add an epsilon to the argument since the gradient of sqrt(0) is unstable.

		# You should return two values: a score for the circle (sum of soft inlier scores), 
		# and a vector with the soft inlier score of each point.

		# -- STUDENT END ----------------------------------------------------------

		eps = 0.000001
		dx = cX - x
		dy = cY - y
		dists = torch.abs( torch.sqrt( dx*dx + dy*dy + eps ) -r )
        
		dists = 1 - torch.sigmoid(self.inlier_beta * (dists - self.inlier_thresh)) 

		print(dists)

		score = torch.sum(dists)
    
		return score, dists

	def __refine_hyp(self, x, y, weights):
		'''
		Refinement by least squares fit.

		x -- vector of x values (range 0 to 1)
		y -- vector of y values (range 0 to 1)
		weights -- vector of weights (1 per point)		
		'''

		# -- STUDENT BEGIN --------------------------------------------------------

		# TASK 4.3. (and 4.4.)

		# You are given all predicted points of the CNN (x, y) and a soft inlier weight for each point (weights).
		# Do a least squares fit to all points with weight > 0.5, or a weighted least squares fit to all points.
		# A description can be found in materials/circle_fit.pdf

		# Note that PyTorch offers a differentiable inverse() function. 

		# You should return three values: x of the circle center, y of the circle center, and the radius

		# -- STUDENT END ----------------------------------------------------------


		xIn = torch.masked_select(x, weights>0.5)
		yIn = torch.masked_select(y, weights>0.5)		        

		meanX = torch.mean(xIn)
		meanY = torch.mean(yIn)      

		u = xIn-meanX
		v = yIn-meanY
		

			  
		Suu = torch.sum(u*u)
		Suv = torch.sum(u*v)
		Svv = torch.sum(v*v)
		Suuu = torch.sum(u*u*u)
		Suvv = torch.sum(u*v*v)
		Svuu = torch.sum(v*u*u)
		Svvv = torch.sum(v*v*v)
      
		A = torch.tensor([[Suu, Suv], [Suv, Svv]])
		B = torch.tensor([[0.5*(Suuu+Suvv)], [0.5*(Svvv+Svuu)]])

		try:
			A = torch.inverse(A)
		except:
			print("num inliner", u.size(0))            
			print(A, B)
			
		c = torch.mm(A,B)
		#c, _ = torch.gesv(B, A)
        
		cX = c[0]+meanX        
		cY = c[1]+meanX
    
		radius = torch.sqrt( c[0]*c[0] + c[1]*c[1] + (Suu+Svv)/u.size(0) )

		return cX, cY, radius
    

	def __call__(self, prediction, labels):
		'''
		Perform robust, differentiable line fitting according to DSAC.

		Returns the expected loss of choosing a good line hypothesis which can be used for backprob.

		prediction -- predicted 2D points for a batch of images, array of shape (Bx2) where
			B is the number of images in the batch
			2 is the number of point dimensions (y, x)
		labels -- ground truth labels for the batch, array of shape (Bx2) where
			B is the number of images in the batch
			2 is the number of parameters (intercept, slope)
		'''

		# working on CPU because of many, small matrices
		prediction = prediction.cpu()

		batch_size = prediction.size(0)

		avg_exp_loss = 0 # expected loss
		avg_top_loss = 0 # loss of best hypothesis

		self.est_parameters = torch.zeros(batch_size, 3) # estimated lines
		self.est_losses = torch.zeros(batch_size) # loss of estimated lines
		self.batch_inliers = torch.zeros(batch_size, prediction.size(2)) # (soft) inliers for estimated lines

		for b in range(0, batch_size):

			hyp_losses = torch.zeros([self.hyps, 1]) # loss of each hypothesis
			hyp_scores = torch.zeros([self.hyps, 1]) # score of each hypothesis

			max_score = 0 	# score of best hypothesis

			y = prediction[b, 0] # all y-values of the prediction
			x = prediction[b, 1] # all x.values of the prediction

			for h in range(0, self.hyps):	
				# === step 1: sample hypothesis ===========================
				cX, cY, r, valid = self.__sample_hyp(x, y)
				if not valid: continue

				# === step 2: score hypothesis using soft inlier count ====
				score, inliers = self.__soft_inlier_count(cX, cY, r, x, y)

				# === step 3: refine hypothesis ===========================
				cX, cY, r = self.__refine_hyp(x, y, inliers)

				hyp = torch.zeros([3])
				hyp[0] = cX
				hyp[1] = cY
				hyp[2] = r

				# === step 4: calculate loss of hypothesis ================
				loss = self.loss_function(hyp, labels[b]) 

				# store results
				hyp_losses[h] = loss
				hyp_scores[h] = score

				# keep track of best hypothesis so far
				if score > max_score:
					max_score = score
					self.est_losses[b] = loss
					self.est_parameters[b] = hyp
					self.batch_inliers[b] = inliers

			# === step 5: calculate the expectation ===========================

			#softmax distribution from hypotheses scores			
			hyp_scores = F.softmax(self.inlier_alpha * hyp_scores, 0)

			# expectation of loss
			exp_loss = torch.sum(hyp_losses * hyp_scores)
			avg_exp_loss = avg_exp_loss + exp_loss

			# loss of best hypothesis (for evaluation)
			avg_top_loss = avg_top_loss + self.est_losses[b]

		return avg_exp_loss / batch_size, avg_top_loss / batch_size
