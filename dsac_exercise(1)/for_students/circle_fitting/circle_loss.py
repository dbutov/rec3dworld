import torch

class CircleLoss:
	'''
	Compares two circle by distance of parameter values.
	'''

	def __init__(self, image_size):
		'''
		Constructor.

		image_size -- size of the input images, used to normalize the loss
		'''
		self.image_size = image_size

	def __call__(self, est, gt):
		'''
		Calculate the circle loss.

		est -- estimated circle, form: [cX, cY, r]
		gt -- ground truth circle, form: [cX, cY, r]
		'''
		
		# -- STUDENT BEGIN --------------------------------------------------------

		# TASK 3.

		# You are given the estimated and ground truth circle (both given as 3-vectors containting 
		# the center coordinate and radius.

		# Calculate the loss as the Euclidean distance between the circle centers plus the absolute difference in radii.
		# Note that the given circle parameters are in relative coordinate, i.e. from 0 to 1. The loss should be in pixels,
		# so multiply by self.image_size

		# You should return one value: the loss.

		# -- STUDENT END ----------------------------------------------------------

		dX= est[0]-gt[0]
		dY= est[1]-gt[1]
		dR= est[2]-gt[2]

		loss = ( torch.sqrt( dX*dX + dY*dY ) + torch.abs(dR) ) * self.image_size

		return loss
