# -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 16:12:45 2018

@author: Hendrik
"""

import numpy as np
import matplotlib.pyplot as plt

log_to_load = "line_fitting\\log_rf65_c4_h64_t0.05.txt"
log_to_load = "result_dimitri_1.txt"
smothing_window_size = 20

# load the log
log_content = np.loadtxt(log_to_load)

# smooth the data
smoothed = np.convolve(log_content[...,3], np.ones((smothing_window_size,))/smothing_window_size, mode='valid')
len_smoothed = len(log_content[...,3]) - smothing_window_size + 1
half_border = (len(log_content[...,3]) - len(smoothed)) // 2
over_flow = (len(log_content[...,3]) - len(smoothed)) % 2

# plot the data
size_factor = 0.75
fig, ax = plt.subplots()
fig.set_size_inches(size_factor*9*4./3., size_factor*9)
plt.plot(log_content[...,0], log_content[...,3], 'x', label="Raw loss")
plt.plot(log_content[...,0][half_border:-(half_border+over_flow)], smoothed, label="Smoothed loss (windows size={})".format(smothing_window_size))
plt.legend()
plt.xlabel("Itteration")
plt.ylabel("Direct loss")
plt.savefig("loss_plot.png")
plt.show()

